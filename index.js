const elBoard = document.querySelector('.board')
const elResults = document.querySelector('.results')
const elMessage = document.querySelector('.message')
const btnRestart = document.querySelector('.btn-restart')

// let board = (() => (new Array(9)).fill([]).map(row => (new Array(3)).fill(' ')))() // cute but too complicated
let board = [[' ',' ',' '],
             [' ',' ',' '],
             [' ',' ',' ']]
let players = ['x', 'o'];
let currentPlayer = players[0];

// draw empty board
function startGame() {
    drawBoard(getBoardString([1, 1], ' '));
    console.group();
}
startGame();


elBoard.addEventListener('click', move);

function move(e) {
    const cellСoordinates = getСoordinates(e);
    if (!isCellEmpty(cellСoordinates)) {
        return; // do nothing if cell is occupied
    }
    drawBoard(getBoardString(cellСoordinates, currentPlayer));

    if (isWin()) {
        endGame(`${currentPlayer} wins!\n`);
    } else if (isDraw()) {
        endGame(`Draw!`);
    } else {
        swapPlayer();
    }
    console.table(board);
}

function getСoordinates(e) {
    const el = e.srcElement;
    const cellW = el.clientWidth / 3;
    const cellH = el.clientHeight / 3;
    const x = Math.floor(e.offsetX / cellW);
    const y = Math.floor(e.offsetY / cellH);
    return [x, y];
}

function isCellEmpty([x, y]) {
    if (board[x][y] == ' ')
        return true;
    return false;
}

function getBoardString([x, y], player) {
    board[x][y] = player;

    return (
`${board[0][0]}|${board[1][0]}|${board[2][0]}
-+-+-
${board[0][1]}|${board[1][1]}|${board[2][1]}
-+-+-
${board[0][2]}|${board[1][2]}|${board[2][2]}`);
}

function drawBoard(board) {
    elBoard.textContent = board;
}

function swapPlayer() {
    if (currentPlayer == players[0])
        currentPlayer = players[1];
    else
        currentPlayer = players[0];
}

function isWin() {
    for (i = 0; i < 3; i++) {
        // row
        if (board[i][0] != ' ' && board[i][0] == board[i][1] && board[i][0] == board[i][2])
            return true;
        // col
        if (board[0][i] != ' ' && board[0][i] == board[1][i] && board[0][i] == board[2][i])
            return true;
    }
    // diagonals
    if (board[0][0] != ' ' && board[0][0] == board[1][1] && board[0][0] == board[2][2])
        return true;
    if (board[0][2] != ' ' && board[0][2] == board[1][1] && board[0][2] == board[2][0])
        return true;

    return false;
}

function isDraw() {
    for (i = 0; i < 3; i++) {
        if (board[i].includes(' '))
            return false; // if any empty cell remains
    }
    return true;
}

function endGame(message) {
    elBoard.classList.add('hide');
    elResults.classList.remove('hide');
    elMessage.textContent = message;
    elBoard.removeEventListener('click', move);

    console.groupEnd();
    console.log(`%c ${message}`, 'font-size: 1.5rem');
}

btnRestart.addEventListener('click', restartGame);

function restartGame() {
    elBoard.classList.remove('hide');
    elResults.classList.add('hide');

    board = board.map(row => row.map(el => el = ' '));
    startGame();
}